import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {"per_page": 1, "query": city + " " + state}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    #  Handle Key error
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # Geocoding to get coordinates
    url = "http://api.openweathermap.org/geo/1.0/direct?"
    params = {
        "q": city + "," + state + "," + "US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    # get current weather using coordinates
    url2 = "https://api.openweathermap.org/data/2.5/weather?"
    params2 = {
        "lat": content[0]["lat"],
        "lon": content[0]["lon"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    headers2 = {"Authorization": OPEN_WEATHER_API_KEY}
    response2 = requests.get(url2, params=params2, headers=headers2)
    content2 = json.loads(response2.content)

    #  Handle Key error
    try:
        return {
            "weather": {
                "temp": content2["main"]["temp"],
                "description": content2["weather"][0]["description"],
            },
        }
    except (KeyError, IndexError):
        return {"weather": "null"}
