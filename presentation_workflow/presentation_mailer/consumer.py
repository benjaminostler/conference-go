import json
import pika
import django
import os
import sys
import time
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def presentation_approval(ch, method, properties, body):
    presentation = json.loads(body)
    send_mail(
        "Your presentation has been accepted",
        f"{presentation['presenter_name']}, we're happy to tell you that your presentation {presentation['title']} has been accepted",
        "admin@conference.go",
        [presentation["presenter_email"]],
        fail_silently=False,
    )


def presentation_rejection(ch, method, properties, body):
    presentation = json.loads(body)
    send_mail(
        "Your presentation has been rejected",
        f"{presentation['presenter_name']}, we're sad to inform you that your presentation {presentation['title']} has been rejected",
        "admin@conference.go",
        [presentation["presenter_email"]],
        fail_silently=False,
    )


# Create a main method to run
def main():
    # Set the hostname that we'll connect to
    parameters = pika.ConnectionParameters(host="rabbitmq")

    # Create a connection to RabbitMQ
    connection = pika.BlockingConnection(parameters)

    # Open a channel to RabbitMQ
    channel = connection.channel()

    # Create a queue if it does not exist
    channel.queue_declare(queue="presentation_approvals")
    channel.queue_declare(queue="presentation_rejections")
    # Configure the consumer to call the process_message function
    # when a message arrives
    channel.basic_consume(
        queue="presentation_approvals",
        on_message_callback=presentation_approval,
        auto_ack=True,
    )
    channel.basic_consume(
        queue="presentation_rejections",
        on_message_callback=presentation_rejection,
        auto_ack=True,
    )
    # Print a status
    print(" [*] Waiting for messages. To exit press CTRL+C")

    # Tell RabbitMQ that you're ready to receive messages
    channel.start_consuming()


# Just extra stuff to do when the script runs
if __name__ == "__main__":
    while True:
        try:
            main()
        except KeyboardInterrupt:
            print("Interrupted")
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)
        except AMQPConnectionError:
            print("Could not connect to RabbitMQ")
            time.sleep(2.0)
